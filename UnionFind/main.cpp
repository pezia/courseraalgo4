#include <iostream>
#include "IUnionFind.h"
#include "QuickFind.h"
#include "QuickUnion.h"
#include "QuickUnionWeighted.h"
#include "QuickUnionWeightedWithPathCompression.h"

using namespace std;

int main() {

	//IUnionFind *uf = new QuickFind(10);
	//IUnionFind *uf = new QuickUnion(10);
	//IUnionFind *uf = new QuickUnionWeighted(10);
	IUnionFind *uf = new QuickUnionWeightedWithPathCompression(10);

	uf->connect(0, 1);

	cout << "0, 1: " << uf->isConnected(0, 1) << "\n";

	cout << "0, 2: " << uf->isConnected(0, 2) << "\n";
	uf->connect(0, 2);
	cout << "0, 2: " << uf->isConnected(0, 2) << "\n";

	cout << "2, 9: " << uf->isConnected(2, 9) << "\n";
	uf->connect(0, 9);
	cout << "2, 9: " << uf->isConnected(2, 9) << "\n";

	delete uf;
	return 0;
}
