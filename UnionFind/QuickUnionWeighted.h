#pragma once
#include "IUnionFind.h"

class QuickUnionWeighted : public IUnionFind
{
private:
	size_t numElements;
	size_t *parents;
	size_t *treeSize;
public:

	QuickUnionWeighted(size_t numElements) : numElements(numElements)
	{
		this->parents = new size_t[numElements];
		this->treeSize = new size_t[numElements];

		for (size_t i = 0; i < numElements; i++){
			this->parents[i] = i;
			this->treeSize[i] = 1;
		}
	}

	~QuickUnionWeighted()
	{
		delete[] this->parents;
		delete[] this->treeSize;
	}

	void connect(size_t p, size_t q) {
		size_t pRoot = this->findRoot(p);
		size_t qRoot = this->findRoot(q);

		if (this->treeSize[pRoot] >= this->treeSize[qRoot]) {
			this->parents[qRoot] = pRoot;
			this->treeSize[pRoot] += this->treeSize[qRoot];
		}
		else {
			this->parents[pRoot] = qRoot;
			this->treeSize[qRoot] += this->treeSize[pRoot];
		}
	}

	bool isConnected(size_t p, size_t q) {
		size_t pRoot = this->findRoot(p);
		size_t qRoot = this->findRoot(q);

		return pRoot == qRoot;
	}

private:
	size_t findRoot(size_t index) {
		size_t currentIndex = index;

		while (currentIndex != this->parents[currentIndex]) {
			currentIndex = this->parents[currentIndex];
		}

		return currentIndex;
	}
};

