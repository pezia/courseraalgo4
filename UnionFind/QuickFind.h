#pragma once
#include "IUnionFind.h"

class QuickFind : public IUnionFind {
private:
	size_t *components;
	size_t numElements;
public:
	QuickFind(size_t numElements) : numElements(numElements) {
		this->components = new size_t[numElements];
		for (size_t i = 0; i < numElements; i++){
			this->components[i] = i;
		}
	}

	~QuickFind() {
		delete[] this->components;
	}

	void connect(size_t p, size_t q) {
		size_t pComponentId = this->components[p];
		size_t qComponentId = this->components[q];

		if (pComponentId == qComponentId) {
			return;
		}

		for (size_t i = 0; i < this->numElements; i++) {
			if (this->components[i] == pComponentId) {
				this->components[i] = qComponentId;
			}
		}
	}

	bool isConnected(size_t p, size_t q) {
		return this->components[p] == this->components[q];
	}
};
