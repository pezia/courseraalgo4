#pragma once

__interface IUnionFind
{
	void connect(size_t p, size_t q);
	bool isConnected(size_t p, size_t q);
};
