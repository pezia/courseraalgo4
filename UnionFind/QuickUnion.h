#pragma once
#include "IUnionFind.h"

class QuickUnion : public IUnionFind
{
private:
	size_t numElements;
	size_t *parents;
public:

	QuickUnion(size_t numElements) : numElements(numElements)
	{
		this->parents = new size_t[numElements];

		for (size_t i = 0; i < numElements; i++){
			this->parents[i] = i;
		}
	}

	~QuickUnion()
	{
		delete[] this->parents;
	}

	void connect(size_t p, size_t q) {
		size_t pRoot = this->findRoot(p);
		size_t qRoot = this->findRoot(q);

		this->parents[pRoot] = qRoot;
	}

	bool isConnected(size_t p, size_t q) {
		size_t pRoot = this->findRoot(p);
		size_t qRoot = this->findRoot(q);
		
		return pRoot == qRoot;
	}

private:
	size_t findRoot(size_t index) {
		size_t currentIndex = index;

		while (currentIndex != this->parents[currentIndex]) {
			currentIndex = this->parents[currentIndex];
		}

		return currentIndex;
	}
};

