#include "stdafx.h"
#include "CppUnitTest.h"
#include "../UnionFind/IUnionFind.h"
#include "../UnionFind/QuickFind.h"
#include "../UnionFind/QuickUnion.h"
#include "../UnionFind/QuickUnionWeighted.h"
#include "../UnionFind/QuickUnionWeightedWithPathCompression.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

#define ELEMENT_COUNT 10

namespace UnionFindTest
{		
	TEST_CLASS(UnionFindTest)
	{
	public:
		TEST_METHOD(TestQuickFind)
		{
			IUnionFind *uf = new QuickFind(ELEMENT_COUNT);
			this->RunTest(uf);
			delete uf;
		}

		TEST_METHOD(TestQuickUnion)
		{
			IUnionFind *uf = new QuickUnion(ELEMENT_COUNT);
			this->RunTest(uf);
			delete uf;
		}


		TEST_METHOD(TestQuickUnionWeighted)
		{
			IUnionFind *uf = new QuickUnionWeighted(ELEMENT_COUNT);
			this->RunTest(uf);
			delete uf;
		}

		TEST_METHOD(TestQuickUnionWeightedWithPathCompression)
		{
			IUnionFind *uf = new QuickUnionWeightedWithPathCompression(ELEMENT_COUNT);
			this->RunTest(uf);
			delete uf;
		}
	private:
		void RunTest(IUnionFind *uf) {
			Assert::IsFalse(uf->isConnected(0,1));
			Assert::IsTrue(uf->isConnected(0, 0));

			uf->connect(0, 1);

			Assert::IsTrue(uf->isConnected(0, 1));

			Assert::IsFalse(uf->isConnected(0, 2));
			uf->connect(0, 2);
			Assert::IsTrue(uf->isConnected(0, 2));

			Assert::IsFalse(uf->isConnected(2, 9));
			uf->connect(0, 9);
			Assert::IsTrue(uf->isConnected(2, 9));
		}
	};
}